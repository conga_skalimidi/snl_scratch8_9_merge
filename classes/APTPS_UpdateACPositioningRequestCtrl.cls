/*************************************************************
@Name: APTPS_UpdateACPositioningRequestCtrl
@Author: Siva Kumar
@CreateDate: 27 October 2021
@Description : This  class is called to update Aircraft Positioning request checkbox
******************************************************************/
public class APTPS_UpdateACPositioningRequestCtrl{
@AuraEnabled
    public static string updateACPositioningRequest(string recordId)
    {
        String msg = '';
        Apttus__APTS_Agreement__c updateAgreement = new Apttus__APTS_Agreement__c (id=recordId,APTPS_Request_Aircraft_Positioning__c=true);
         try {
                system.debug('updateAgreement -->'+updateAgreement );
                update updateAgreement;
                msg = 'Success';
            } catch(Exception e) {
                msg = 'Fail';
            }
        return msg;
    }
}