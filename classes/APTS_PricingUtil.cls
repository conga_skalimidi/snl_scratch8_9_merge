public class APTS_PricingUtil {
    
    public static decimal getRequestedHours(Apttus_Config2__LineItem__c item){
        
        system.debug('In APTS_PricingUtil.getRequestedHours');
        
        boolean isOverrideHoursToUnexpire = item.Apttus_Config2__AttributeValueId__r.Override_Requested_Hours__c != null && 
            item.Apttus_Config2__AttributeValueId__r.Override_Requested_Hours__c == TRUE ? TRUE : FALSE;
        
        decimal overriddenRequestedHours = item.Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c != null && 
            item.Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c > 0 ? item.Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c : 0;
        
        decimal requestedHours = !isOverrideHoursToUnexpire && item.Apttus_Config2__AttributeValueId__r.Requested_Hours__c != null && 
            item.Apttus_Config2__AttributeValueId__r.Requested_Hours__c > 0 ? item.Apttus_Config2__AttributeValueId__r.Requested_Hours__c : overriddenRequestedHours;
        
        system.debug('requestedHours-->'+requestedHours);
        return requestedHours;
    }
    
    public static decimal getUnexpiredHours(Apttus_Config2__LineItem__c item){
        
        system.debug('In APTS_PricingUtil.getUnexpiredHours');
        
        boolean isOverrideHoursToUnexpire =  item.Apttus_Config2__AttributeValueId__r.APTS_Override_Hours_To_Unexpire__c != null && 
            item.Apttus_Config2__AttributeValueId__r.APTS_Override_Hours_To_Unexpire__c == TRUE ? TRUE : FALSE;
        
        decimal hoursToUnexpire = item.Apttus_Config2__AttributeValueId__r.APTS_Hours_To_Unexpire__c != null && 
            item.Apttus_Config2__AttributeValueId__r.APTS_Hours_To_Unexpire__c > 0 ? item.Apttus_Config2__AttributeValueId__r.APTS_Hours_To_Unexpire__c :0;
        
        decimal unexpiredHours = isOverrideHoursToUnexpire && item.Apttus_Config2__AttributeValueId__r.APTS_Overridden_Hours_To_Unexpire__c != null && 
            item.Apttus_Config2__AttributeValueId__r.APTS_Overridden_Hours_To_Unexpire__c > 0 ? 
            item.Apttus_Config2__AttributeValueId__r.APTS_Overridden_Hours_To_Unexpire__c : hoursToUnexpire;
        
        system.debug('unexpiredHourss-->'+unexpiredHours);
        return unexpiredHours;
    }
    
    public static decimal getExpiredHours(Apttus_Config2__LineItem__c item){
        
        system.debug('In APTS_PricingUtil.getExpiredHours');
        
        decimal aircraftExpiredHours = item.Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c != null && 
            item.Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c > 0 ? item.Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c :0;
        
        system.debug('aircraftExpiredHours-->'+aircraftExpiredHours);
        return aircraftExpiredHours;
    }
    
    public static boolean isEuropeProgram(Apttus_Config2__LineItem__c item){
        
        system.debug('In APTS_PricingUtil.isEuropeProgram');
        
        boolean result = false;
        
        if(item.Apttus_Config2__LineType__c != null && item.Apttus_Config2__LineType__c == 'Product/Service')
            result = (item.Apttus_Config2__ProductId__c != null && item.Apttus_Config2__ProductId__r.Program__c != null && 
                      item.Apttus_Config2__ProductId__r.Program__c == APTS_ConstantUtil.NJ_EUROPE_PROGRAM) ? TRUE : FALSE;
        
        else if(item.Apttus_Config2__LineType__c != null && item.Apttus_Config2__LineType__c == 'Option')
            result = (item.Apttus_Config2__OptionId__c != null && item.Apttus_Config2__OptionId__r.Program__c != null && 
                      item.Apttus_Config2__OptionId__r.Program__c == APTS_ConstantUtil.NJ_EUROPE_PROGRAM) ? TRUE : FALSE; 
        
        system.debug('result-->'+result);
        return result;
    }
    
    public static boolean isParentLine(Apttus_Config2__LineItem__c item){

        system.debug('In APTS_PricingUtil.isParentLine');
    
        boolean result = false;
        if(item.Apttus_Config2__LineType__c != null && item.Apttus_Config2__LineType__c == 'Product/Service')
            result = true;
        
        system.debug('isParentLine result-->'+result);
        return result;
    }
    
    public static boolean isNewOption(Apttus_Config2__LineItem__c item){

        system.debug('In APTS_PricingUtil.isNewOption');
    
        boolean result = false;
        if(item.Apttus_Config2__LineType__c != null && item.Apttus_Config2__LineType__c == 'Option' && 
        item.Apttus_Config2__LineStatus__c != null && item.Apttus_Config2__LineStatus__c == 'New')
            result = true;
        
        system.debug('isNewOption result-->'+result);
        return result;
    }
    
    public static boolean isPurchasePriceCT(Apttus_Config2__LineItem__c item){

        system.debug('In APTS_PricingUtil.isPurchasePriceCT');
    
        boolean result = false;
        if(item.Apttus_Config2__ChargeType__c != null && item.Apttus_Config2__ChargeType__c == 'Purchase Price')
            result = true;
        
        system.debug('isPurchasePriceCT result-->'+result);
        return result;
    }

    public static boolean isHalfCardPremiumCT(Apttus_Config2__LineItem__c item){

        system.debug('In APTS_PricingUtil.isHalfCardPremiumCT');
    
        boolean result = false;
        if(item.Apttus_Config2__ChargeType__c != null && item.Apttus_Config2__ChargeType__c == 'Half Card Premium')
            result = true;
        
        system.debug('isHalfCardPremiumCT result-->'+result);
        return result;
    }
    
    public static boolean isComboCardPremiumCT(Apttus_Config2__LineItem__c item){

        system.debug('In APTS_PricingUtil.isComboCardPremiumCT');
    
        boolean result = false;
        if(item.Apttus_Config2__ChargeType__c != null && item.Apttus_Config2__ChargeType__c == 'Combo Card Premium')
            result = true;
        
        system.debug('isComboCardPremiumCT result-->'+result);
        return result;
    }
    
    public static boolean isOption(Apttus_Config2__LineItem__c item){

        system.debug('In APTS_PricingUtil.isOption');
    
        boolean result = false;
        if(item.Apttus_Config2__LineType__c != null && item.Apttus_Config2__LineType__c == 'Option')
            result = true;
        
        system.debug('isOption result-->'+result);
        return result;
    }
    
    public static boolean isAircraftFamilyType(Apttus_Config2__LineItem__c item){

        system.debug('In APTS_PricingUtil.isAircraftFamilyType');
    
        boolean result = false;
        if(item.Apttus_Config2__OptionId__r.Family != null && item.Apttus_Config2__OptionId__r.Family == 'Aircraft')
            result = true;
        
        system.debug('isAircraftFamilyType result-->'+result);
        return result;
    }
    
    public static Decimal getHigherCabinRank(Decimal r1, Decimal r2){
        
        system.debug('In APTS_PricingUtil.getHigherCabinRank');
        
        if(r1 == null && r2 == null)
            return 0;
        else if(r1 == null && r2 != null)
            return r2;
        else if (r2 == null && r1 != null)
            return r1;
        else 
            return r1 > r2 ? r1 : r2;
    }
    
    public static string getRelatedChargeType(string chargeType){
        
        system.debug('In APTS_PricingUtil.getRelatedChargeType chargeType-->'+chargeType);
        
        string result = '';
        
        switch on chargeType{
            when 'Federal Excise Tax'{
                result = 'Purchase Price';
            }
            when 'Half Card Premium FET'{
                result = 'Half Card Premium';
            }
            when 'Combo Card Premium FET'{
                result = 'Combo Card Premium';
            }
            when else {
                result = '';
            }
        }
        return result;
    }
    
    public static Map<decimal, decimal> updateAircraftHoursMap(Map<decimal, decimal> currentMap, Apttus_Config2__LineItem__c item){
        system.debug('In APTS_PricingUtil.updateAircraftHoursMap currentMap-->'+currentMap);
        Decimal lineNumber = item.Apttus_Config2__LineNumber__c;
        Decimal aircraftHours = item.Apttus_Config2__AttributeValueId__r.Aircraft_Hours__c  != null ? 
            item.Apttus_Config2__AttributeValueId__r.Aircraft_Hours__c : 0;
        if(lineNumber != null && aircraftHours != null){
            if(currentMap.isEmpty()){
                currentMap.put(lineNumber, aircraftHours);
            }else{
                Decimal existingAH = currentMap.get(lineNumber) != null ? currentMap.get(lineNumber) : 0;
                currentMap.put(lineNumber, existingAH+aircraftHours);
            }
        }
        system.debug('currentMap AFTER-->'+currentMap);
        return currentMap;
    }
    
    public static Map<decimal, decimal> updateRMNGAircraftHoursMap(Map<decimal, decimal> currentMap, Apttus_Config2__LineItem__c item){
        system.debug('In APTS_PricingUtil.updateRMNGAircraftHoursMap currentMap-->'+currentMap);
        Decimal lineNumber = item.Apttus_Config2__LineNumber__c;
        Decimal aircraftRemainingHours = item.Apttus_Config2__AttributeValueId__r.Aircraft_Remaining_Hours__c != null ? 
            item.Apttus_Config2__AttributeValueId__r.Aircraft_Remaining_Hours__c : 0;
        if(lineNumber != null && aircraftRemainingHours != null){
            if(currentMap.isEmpty()){
                currentMap.put(lineNumber,aircraftRemainingHours);
            }else{
                Decimal existingHours = currentMap.get(lineNumber) != null ? currentMap.get(lineNumber) : 0;
                currentMap.put(lineNumber, existingHours+aircraftRemainingHours);
            }
        }
        system.debug('currentMap AFTER-->'+currentMap);
        return currentMap;
    }
    
    public static Map<decimal, decimal> updateEXPAircraftHoursMap(Map<decimal, decimal> currentMap, Apttus_Config2__LineItem__c item){
        system.debug('In APTS_PricingUtil.updateEXPAircraftHoursMap currentMap-->'+currentMap);
        Decimal lineNumber = item.Apttus_Config2__LineNumber__c;
        Decimal aircraftEXPHours = getExpiredHours(item);
        if(lineNumber != null && aircraftEXPHours != null){
            if(currentMap.isEmpty()){
                currentMap.put(lineNumber, aircraftEXPHours);
            }else{
                Decimal existingHours = currentMap.get(lineNumber) != null ? currentMap.get(lineNumber) : 0;
                currentMap.put(lineNumber, existingHours+aircraftEXPHours);
            }
        }
        system.debug('currentMap AFTER-->'+currentMap);
        return currentMap;
    }
    
    public static boolean isAdjustmentApplied(Apttus_Config2__LineItem__c item){

        system.debug('In APTS_PricingUtil.isAdjustmentApplied');
    
        boolean result = false;
        if(!String.isBlank(item.Apttus_Config2__AdjustmentType__c) && item.Apttus_Config2__NetAdjustmentPercent__c != 0)
            result = true;
        
        system.debug('isAdjustmentApplied result-->'+result);
        return result;
    }
    
    public static boolean isAddEnhModification(string ModificationType){
        system.debug('In APTS_PricingUtil.isAddEnhModification');
    
        boolean result = false;
        if(ModificationType != null && ModificationType == APTS_ConstantUtil.ADD_ENHANCEMENTS)
            result = true;
        
        system.debug('isAddEnhModification result-->'+result);
        return result;
    }
    
    public static boolean isRedemptionPT(Apttus_Config2__LineItem__c item){
        system.debug('In APTS_PricingUtil.isRedemptionPT');
    
        boolean result = false;
        if(item.Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c != null && 
               item.Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c == APTS_ConstantUtil.REDEMPTION_PRODUCT_TYPE)
            result = true;
        
        system.debug('isRedemptionPT result-->'+result);
        return result;
    }
    
    public static boolean isComboPT(Apttus_Config2__LineItem__c item){
        system.debug('In APTS_PricingUtil.isComboPT');
    
        boolean result = false;
        //START: GCM-9542
        //This condition is updated to check if selected product type is Combo OR Elite Combo or Classic Combo
        if(item.Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c != null && 
           (item.Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c == APTS_ConstantUtil.COMBO_PRODUCT_TYPE || 
           APTS_ConstantUtil.ELITE_COMBO_PRODUCT_TYPE.equalsIgnoreCase(item.Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c) || 
           APTS_ConstantUtil.CLASSIC_COMBO_PRODUCT_TYPE.equalsIgnoreCase(item.Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c) || 
           APTS_ConstantUtil.COMBO_PRODUCT_NJE.equalsIgnoreCase(item.Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c))) //GCM-8679: Added condition for NJE COMBO
            result = true;
        //END: GCM-9542
        system.debug('isComboPT result-->'+result);
        return result;
    }
    
    public static string getProductType(Apttus_Config2__LineItem__c item){
        system.debug('In APTS_PricingUtil.getProductType');
    
        string result = '';
        if(item.Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c != null)
            result = item.Apttus_Config2__AttributeValueId__r.APTS_Selected_Product_Type__c;
        
        system.debug('getProductType result-->'+result);
        return result;
    }

    public static void ProcessPAVandLineITem (Id cartId, Id accountId) {
        //process  cross account assignment transaction
        //Update PAV with  APAV
        updatePAVandLIStatus (cartId, accountId);
 
        //update FET,CFET, HCFET with appropriate Asset Line ITem
        updateFETAndComboFETLineItem(cartId);


    }
    

    @future(callout=true)
    public static void updateFETAndComboFETLineItem (Id cartId) {
    
        //system.debug('into APTS_PricingUtil updateFETAndComboFETLineItem');
        List<Apttus_Config2__LineItem__c> listLineItems = new List<Apttus_Config2__LineItem__c>();
        Apttus_Config2__LineItem__c oLineItem;

        listLineItems = [Select Apttus_Config2__LineType__c, 
                            Apttus_Config2__LineStatus__c,
                            Apttus_Config2__AssetLineItemId__r.Id,
                            Apttus_Config2__AssetLineItemId__r.Apttus_Config2__BundleAssetId__r.Id,
                            Apttus_Config2__ConfigurationId__r.Apttus_Config2__AccountId__r.Id
                        From Apttus_Config2__LineItem__c
                        Where Apttus_Config2__ConfigurationId__c = :cartID 
                        And Apttus_Config2__LineType__c = 'Product/Service'
                        And Apttus_Config2__ChargeType__c = 'Purchase Price'
                ];

        //system.debug('listLineItems-->'+listLineItems);

        if (listLineItems!=null && listLineItems.size() > 0){

            oLineItem = listLineItems.get (0);
            List<Apttus_Config2__LineItem__c> allLi = new List<Apttus_Config2__LineItem__c>();
            List<Apttus_Config2__LineItem__c> updateALI = new List<Apttus_Config2__LineItem__c>();

            allLi = [Select Apttus_Config2__LineType__c, 
                        Apttus_Config2__ChargeType__c,
                        Apttus_Config2__AssetLineItemId__r.Id, Apttus_Config2__AssetLineItemId__r.Apttus_Config2__BillToAccountId__c, 
                        Apttus_Config2__AssetLineItemId__r.Apttus_Config2__ShipToAccountId__c,
                        Apttus_Config2__BillToAccountId__c,
                        Apttus_Config2__ShipToAccountId__c
                    From Apttus_Config2__LineItem__c
                    Where Apttus_Config2__ConfigurationId__c = :cartID 
                    And Apttus_Config2__LineType__c = 'Product/Service'
                    And Apttus_Config2__ChargeType__c IN ('Federal Excise Tax', 'Half Card Premium FET', 'Combo Card Premium FET', 'Combo Card Premium', 'Half Card Premium')];

            for(Apttus_Config2__LineItem__c li : allLi){

                    //system.debug('FET Ali-->' + fetAlis[0].Id);
                    li.Apttus_Config2__AssetLineItemId__c = li.Apttus_Config2__AssetLineItemId__r.Id;
                    li.Apttus_Config2__BillToAccountId__c = li.Apttus_Config2__AssetLineItemId__r.Apttus_Config2__BillToAccountId__c;
                    li.Apttus_Config2__ShipToAccountId__c = li.Apttus_Config2__AssetLineItemId__r.Apttus_Config2__ShipToAccountId__c;
                    updateALI.add(li);
                
            }
            update updateALI;
       
        }

   
    }

    public static void updatePAVandLIStatus (Id cartId, Id accountId) {
    
        //system.debug('into updatePAVandLIStatus');

        List<Apttus_Config2__LineItem__c> lineStatusItems = new List<Apttus_Config2__LineItem__c>();
        List<Apttus_Config2__LineItem__c> listLineItems = new List<Apttus_Config2__LineItem__c>();

        List<Apttus_Config2__ProductAttributeValue__c> listOfPAVtoUpdate = new List<Apttus_Config2__ProductAttributeValue__c>();

        Map<Id, Id> attrPAVtoAPAVMap = new Map<Id, Id>();

        lineStatusItems = [SELECT id,Apttus_Config2__AssetLineItemId__c,Apttus_Config2__AttributeValueId__c, Apttus_Config2__LineType__c, Apttus_Config2__LineStatus__c
        FROM Apttus_Config2__LineItem__c 
        WHERE Apttus_Config2__ConfigurationId__c =:cartID 
        AND Apttus_Config2__BillToAccountId__c = :accountId];

        for(Apttus_Config2__LineItem__c li : lineStatusItems){
                //system.debug('li.Apttus_Config2__LineType__c -->'+ li.Apttus_Config2__LineType__c + 'li.Apttus_Config2__LineStatus__c -->'+ li.Apttus_Config2__LineStatus__c);

                if(li.Apttus_Config2__LineType__c == 'Product/Service')
                    li.Apttus_Config2__LineStatus__c = 'Amended';
                else
                    li.Apttus_Config2__LineStatus__c = 'Existing';
                
        }

        try{
            update lineStatusItems;
        }catch(Exception ex){
            system.debug('Failed to update line item status. Method -->updatePAVwithAssePAV. '+ 'Exception --> '+ +ex.getMessage());
        }

        listLineItems = [SELECT id,Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__c,Apttus_Config2__AttributeValueId__c, 
                        Apttus_Config2__LineType__c, Apttus_Config2__LineStatus__c
                    FROM Apttus_Config2__LineItem__c 
                    WHERE Apttus_Config2__ConfigurationId__c =:cartID 
                        AND Apttus_Config2__BillToAccountId__c = :accountId];

        for(Apttus_Config2__LineItem__c li : listLineItems){
            if(!attrPAVtoAPAVMap.containsKey(li.Apttus_Config2__AttributeValueId__c)){
            attrPAVtoAPAVMap.put(li.Apttus_Config2__AttributeValueId__c, li.Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__c);

            }
        }

        // system.debug('Unique AssetAttrID  -->'+ attrPAVtoAPAVMap);

        if(!attrPAVtoAPAVMap.IsEmpty()){
            //system.debug('into IF attrPAVtoAPAVMap');
            for(Apttus_Config2__ProductAttributeValue__c iPav : [SELECT Id, Aircraft_Hours__c,Aircraft_Remaining_Hours__c,
            Aircraft_All_Inclusive_Hourly_Rate__c, All_Inclusive__c, APTPS_Aggregated_Hourly_Rate__c, APTS_All_Inclusive_Blended_Rate__c, APTS_Blended_Rate__c,
            APTS_Override_Hours_To_Unexpire__c, APTS_Waive_FET__c, APTS_Bonus_Hours__c,APTPS_Extra_Hours__c,
            APTPS_Extra_Hours_AE__c,Non_Standard__c, Usage_Type__c,APTS_Number_of_Upgrades__c, APTS_Number_of_Flight_Segments__c,
            From_Aircraft_Type__c,To_Aircraft_Type__c, Interchange_Type__c, Total_Hours_Not_to_Exceed__c, APTS_Current_Value__c,
            Prepaid_Incidentals__c, APTS_Split_Payment__c, APTS_Refund_After_First_Flight__c, Death_or_Disability_Clause__c,
            APTS_Death_or_Disability_Recipient__c, APTS_Death_or_Disability_Recipient_2__c, Business_Development_Partnership__c,
            Amount__c, Comments__c, APTS_Referred_Prospect_Hours__c, APTS_Referring_Owner_Account__c, APTS_Referring_Owner_Hours__c,
            APTS_PPD_Exclusion_Date__c, APTS_Aircraft_Type_product__c, APTS_Cabin_Class_NJUS__c, APTS_Cabin_Class_NJE__c,
            APTS_Number_of_Waivers_SLW__c, APTS_NJE_Service_Area_Upgrade__c, NJE_Service_Area__c, Short_Leg_Waivers_Comments__c,
            APTS_NJUS_Service_Area_SLW__c, NJUS_Service_Area__c, Partnership__c

            FROM Apttus_Config2__ProductAttributeValue__c WHERE Id IN :attrPAVtoAPAVMap.keySet()]) {
               
                if(attrPAVtoAPAVMap.containsKey(iPav.Id)){
                    //system.debug('into contians if attrPAVtoAPAVMap');
                    Apttus_Config2__AssetAttributeValue__c iAssetPav = [SELECT Id, Aircraft_Hours__c, Aircraft_Remaining_Hours__c,
                    Aircraft_All_Inclusive_Hourly_Rate__c, All_Inclusive__c,APTPS_Aggregated_Hourly_Rate__c, APTS_All_Inclusive_Blended_Rate__c, APTS_Blended_Rate__c,
                    APTS_Override_Hours_To_Unexpire__c, APTS_Waive_FET__c, APTS_Bonus_Hours__c,APTPS_Extra_Hours__c,
                    APTPS_Extra_Hours_AE__c, Non_Standard__c,Usage_Type__c, APTS_Number_of_Upgrades__c, APTS_Number_of_Flight_Segments__c,
                    From_Aircraft_Type__c, To_Aircraft_Type__c, Interchange_Type__c, Total_Hours_Not_to_Exceed__c, Current_Value__c,
                    Prepaid_Incidentals__c, APTS_Split_Payment__c, APTS_Refund_After_First_Flight__c, Death_or_Disability_Clause__c,
                    APTS_Death_or_Disability_Recipient__c, APTS_Death_or_Disability_Recipient_2__c, Business_Development_Partnership__c,
                    Amount__c, Comments__c, APTS_Referred_Prospect_Hours__c, APTS_Referring_Owner_Account__c, APTS_Referring_Owner_Hours__c,
                    APTS_PPD_Exclusion_Date__c, APTS_Aircraft_Type_product__c, APTS_Cabin_Class_NJUS__c, APTS_Cabin_Class_NJE__c,
                    APTS_Number_of_Waivers_SLW__c, APTS_NJE_Service_Area_Upgrade__c, NJE_Service_Area__c, Short_Leg_Waivers_Comments__c,
                    APTS_NJUS_Service_Area_SLW__c, NJUS_Service_Area__c, Partnership__c
                    FROM Apttus_Config2__AssetAttributeValue__c WHERE Id =: attrPAVtoAPAVMap.get(iPav.Id)];

                    if(iAssetPav != null){

                        system.debug('into AssetPAV--->'+iAssetPav);
                        system.debug('AssetPAV Id--->'+iAssetPav.Id);
                        iPav.Aircraft_Hours__c = iAssetPav.Aircraft_Hours__c;
                        iPav.Aircraft_Remaining_Hours__c = iAssetPav.Aircraft_Remaining_Hours__c;
                        iPav.Aircraft_All_Inclusive_Hourly_Rate__c = iAssetPav.Aircraft_All_Inclusive_Hourly_Rate__c;
                        iPav.All_Inclusive__c = iAssetPav.All_Inclusive__c;
                        iPav.APTPS_Aggregated_Hourly_Rate__c = iAssetPav.APTPS_Aggregated_Hourly_Rate__c;
                        iPav.APTS_All_Inclusive_Blended_Rate__c = iAssetPav.APTS_All_Inclusive_Blended_Rate__c;
                        iPav.APTS_Blended_Rate__c = iAssetPav.APTS_Blended_Rate__c;
                        iPav.APTS_Override_Hours_To_Unexpire__c = iAssetPav.APTS_Override_Hours_To_Unexpire__c;
                        iPav.APTS_Waive_FET__c = iAssetPav.APTS_Waive_FET__c;
                        iPav.APTS_Bonus_Hours__c = iAssetPav.APTS_Bonus_Hours__c;
                        iPav.APTPS_Extra_Hours__c = iAssetPav.APTPS_Extra_Hours__c;
                        iPav.APTS_Bonus_Hours__c = iAssetPav.APTS_Bonus_Hours__c;
                        iPav.APTPS_Extra_Hours_AE__c = iAssetPav.APTPS_Extra_Hours_AE__c;
                        iPav.Non_Standard__c = iAssetPav.Non_Standard__c;
                        iPav.Usage_Type__c = iAssetPav.Usage_Type__c;
                        iPav.APTS_Number_of_Upgrades__c = iAssetPav.APTS_Number_of_Upgrades__c;
                        iPav.APTS_Number_of_Flight_Segments__c = iAssetPav.APTS_Number_of_Flight_Segments__c;
                        iPav.From_Aircraft_Type__c = iAssetPav.From_Aircraft_Type__c;
                        iPav.To_Aircraft_Type__c = iAssetPav.To_Aircraft_Type__c;
                        iPav.Interchange_Type__c = iAssetPav.Interchange_Type__c;
                        iPav.Total_Hours_Not_to_Exceed__c = iAssetPav.Total_Hours_Not_to_Exceed__c;
                        iPav.Prepaid_Incidentals__c = iAssetPav.Prepaid_Incidentals__c;
                        iPav.APTS_Split_Payment__c = iAssetPav.APTS_Split_Payment__c;
                        iPav.Death_or_Disability_Clause__c = iAssetPav.Death_or_Disability_Clause__c;
                        iPav.APTS_Death_or_Disability_Recipient__c = iAssetPav.APTS_Death_or_Disability_Recipient__c;
                        iPav.APTS_Death_or_Disability_Recipient_2__c = iAssetPav.APTS_Death_or_Disability_Recipient_2__c;
                        iPav.Business_Development_Partnership__c = iAssetPav.Business_Development_Partnership__c;
                        iPav.Amount__c = iAssetPav.Amount__c;
                        iPav.Comments__c = iAssetPav.Comments__c;
                        iPav.APTS_Referred_Prospect_Hours__c = iAssetPav.APTS_Referred_Prospect_Hours__c;
                        iPav.APTS_Referring_Owner_Account__c = iAssetPav.APTS_Referring_Owner_Account__c;
                        iPav.APTS_Referring_Owner_Hours__c = iAssetPav.APTS_Referring_Owner_Hours__c;
                        iPav.APTS_PPD_Exclusion_Date__c = iAssetPav.APTS_PPD_Exclusion_Date__c;
                        iPav.APTS_Aircraft_Type_product__c = iAssetPav.APTS_Aircraft_Type_product__c;
                        iPav.APTS_Cabin_Class_NJUS__c = iAssetPav.APTS_Cabin_Class_NJUS__c;
                        iPav.APTS_Cabin_Class_NJE__c = iAssetPav.APTS_Cabin_Class_NJE__c;
                        iPav.APTS_Number_of_Waivers_SLW__c = iAssetPav.APTS_Number_of_Waivers_SLW__c;
                        iPav.APTS_NJE_Service_Area_Upgrade__c = iAssetPav.APTS_NJE_Service_Area_Upgrade__c;
                        iPav.NJE_Service_Area__c = iAssetPav.NJE_Service_Area__c;
                        iPav.Short_Leg_Waivers_Comments__c = iAssetPav.Short_Leg_Waivers_Comments__c;
                        iPav.APTS_NJUS_Service_Area_SLW__c = iAssetPav.APTS_NJUS_Service_Area_SLW__c;
                        iPav.NJUS_Service_Area__c = iAssetPav.NJUS_Service_Area__c;
                        iPav.Partnership__c = iAssetPav.Partnership__c;
                        listOfPAVtoUpdate.add(iPav);

                        system.debug('after PAV--->'+iPav);
                    }

                    
                    system.debug('iPAV Id--->'+iPav.Id);
                }
            }

        }

       
        try{
            update listOfPAVtoUpdate;
        }catch(Exception ex){
            system.debug('Failed to update PAV record. Method -->updatePAVwithAssePAV. '+ 'Exception --> '+ +ex.getMessage());
        }
           
    }

    @future(callout=true)
    public static void removeOrphanTLineItem (Id cartId, Id accountId) {

         List<Apttus_Config2__LineItem__c> toDel = new List<Apttus_Config2__LineItem__c>();
        toDel = [SELECT id FROM Apttus_Config2__LineItem__c WHERE Apttus_Config2__ConfigurationId__c =:cartId 
            AND Apttus_Config2__BillToAccountId__c <> :accountId];
        delete toDel;
    
    }
    
    public static boolean isReferralRefereeLine(Apttus_Config2__LineItem__c item){
        system.debug('In APTS_PricingUtil.isReferralRefereeLine');
    
        boolean result = false;
        if(item.APTPS_Referral_Referee_Type__c != null && 
            (item.APTPS_Referral_Referee_Type__c == APTS_ConstantUtil.REFEREE_LINE || item.APTPS_Referral_Referee_Type__c == APTS_ConstantUtil.REFERRAL_LINE))
            result = true;
        
        system.debug('isReferralRefereeLine result-->'+result);
        return result;
    }

    public static boolean isMMFCT(Apttus_Config2__LineItem__c item){

        system.debug('In APTS_PricingUtil.isMMFCT');
    
        boolean result = false;
        if(item.Apttus_Config2__ChargeType__c != null && 
        APTS_ConstantUtil.MMF_CHARGE_TYPE.equalsIgnoreCase(item.Apttus_Config2__ChargeType__c))
            result = true;
        
        system.debug('isMMFCT result-->'+result);
        return result;
    }
    
    public static boolean isOHFCT(Apttus_Config2__LineItem__c item){

        system.debug('In APTS_PricingUtil.isOHFCT');
    
        boolean result = false;
        if(item.Apttus_Config2__ChargeType__c != null && 
        //(APTS_ConstantUtil.OHF_CHARGE_TYPE.equalsIgnoreCase(item.Apttus_Config2__ChargeType__c) || 
        (APTS_ConstantUtil.OHF_CHARGE_TYPE_1.equalsIgnoreCase(item.Apttus_Config2__ChargeType__c) || 
        APTS_ConstantUtil.OHF_CHARGE_TYPE_2.equalsIgnoreCase(item.Apttus_Config2__ChargeType__c) || 
        APTS_ConstantUtil.OHF_CHARGE_TYPE_3.equalsIgnoreCase(item.Apttus_Config2__ChargeType__c) || 
        APTS_ConstantUtil.OHF_CHARGE_TYPE_4.equalsIgnoreCase(item.Apttus_Config2__ChargeType__c)))
            result = true;
        
        system.debug('isOHFCT result-->'+result);
        return result;
    }
    
}