/************************************************************************************************************************
@Name: APTPS_Approvals
@Author: Conga PS Dev Team
@CreateDate: 25 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_Approvals implements APTPS_ApprovalsCallable {
    private static String metadataName = 'APTPS_Approvals_Registry__mdt';
    public static Map<String, String> prodCodeClassNameMap = new Map<String, String>();
    public static Map<String, String> prodCodeFieldsMap = new Map<String, String>();
    
    
    public APTPS_Approvals() {
        setProdCodeClassNameMap();
       
    }
    
    /** 
    @description: Clear Approvals Flags on Proposal
    @param: Proposals List
    @return: 
    */
    
    private void clearApprovalFlags(List<Apttus_Proposal__Proposal__c> proposalsInScope) {
        if (proposalsInScope != null && proposalsInScope.size() > 0){
            for (Apttus_Proposal__Proposal__c proposalObj : proposalsInScope){
                proposalObj.Requires_Legal_Approval__c = false;
                proposalObj.APTPS_Requires_Demo_Team_Approval__c = false;
                proposalObj.APTPS_Requires_Director_AR_Approval__c = false;
                proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = false;
                proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
                proposalObj.APTPS_Requires_Sales_Ops_Team_Approval__c = false;
                proposalObj.APTS_Requires_RVP_Approval__c = false;              
                proposalObj.APTPS_Requires_DDC_Approval__c = false;              
            }
        }
    }
    
    /** 
    @description: Set Approvals Flags on Proposal
    @param: Proposals List
    @return: 
    */
    
    public void setProposalApprovedStatus(List<Apttus_Proposal__Proposal__c> proposalsInScope) {        
        if (proposalsInScope != null && proposalsInScope.size() > 0){
            for (Apttus_Proposal__Proposal__c proposalObj : proposalsInScope){
                Boolean proposalApprovedStatus = true;
                if (proposalObj.Requires_Legal_Approval__c || proposalObj.APTPS_Requires_Demo_Team_Approval__c || proposalObj.APTPS_Requires_Director_AR_Approval__c || proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c || proposalObj.APTS_Requires_Sales_Director_Approval__c || proposalObj.APTPS_Requires_Sales_Ops_Team_Approval__c || proposalObj.APTS_Requires_RVP_Approval__c || proposalObj.APTPS_Requires_DDC_Approval__c) {
                    proposalApprovedStatus = false;
                    proposalObj.Apttus_Proposal__Approval_Stage__c = APTS_Constants.STATUS_APPROVAL_REQUIRED ;
                    proposalObj.Apttus_QPApprov__Approval_Status__c = APTS_Constants.STATUS_APPROVAL_REQUIRED ;
                    proposalObj.Proposal_Chevron_Status__c = APTS_ConstantUtil.PROPOSAL_SOLUTION;
                }
                if (proposalApprovedStatus) {
                    proposalObj.Apttus_Proposal__Approval_Stage__c = APTS_Constants.STATUS_APPROVED ;
                    proposalObj.Apttus_QPApprov__Approval_Status__c = APTS_Constants.STATUS_APPROVED;
                }
                system.debug('proposalObj-->'+proposalObj);
            }
        }
    }
   
    /** 
    @description: Update Approvals Users from Account Team
    @param: Proposals List
    @return: 
    */
    
    private void updateApproversOnProposal(List<Apttus_Proposal__Proposal__c> proposalsInScope)
    {
        List<Id> accountIds = new List<Id>();
        Map<Id,List<AccountTeamMember>> mapAccountTeamMemberMap = new Map<Id,List<AccountTeamMember>>();
        if (proposalsInScope != null && proposalsInScope.size() > 0){
            for (Apttus_Proposal__Proposal__c proposalObj : proposalsInScope){
                accountIds.add(proposalObj.Apttus_Proposal__Account__c);
            }
        }
        List<AccountTeamMember> allAccountTeamMembers = [SELECT UserId,TeamMemberRole,AccountId FROM AccountTeamMember WHERE AccountId IN :accountIds];

        if (allAccountTeamMembers != null && allAccountTeamMembers.size() > 0){
            for (AccountTeamMember accountTeamMemberObj : allAccountTeamMembers){
                if (mapAccountTeamMemberMap.containsKey(accountTeamMemberObj.AccountId)){
                    mapAccountTeamMemberMap.get(accountTeamMemberObj.AccountId).add(accountTeamMemberObj);
                }
                else {
                    mapAccountTeamMemberMap.put(accountTeamMemberObj.AccountId,new List<AccountTeamMember> {accountTeamMemberObj});
                }
            }
        }

        if (proposalsInScope != null && proposalsInScope.size() > 0){
            for (Apttus_Proposal__Proposal__c proposalObj : proposalsInScope){
                List<AccountTeamMember> accountTeamMembers = mapAccountTeamMemberMap.get(proposalObj.Apttus_Proposal__Account__c);
                if (accountTeamMembers != null && accountTeamMembers.size() > 0){
                    Id salesExecUserId = null;
                    Id accountExecUserId = null;
                    Id regionalVpUserId = null;
                    Id secondarySEUserId = null;
                    Id salesConsultantUserId = null;
                    Id salesDirectorUserId = null;
        
                    for (AccountTeamMember accountTeamMember : accountTeamMembers){
                        if (accountTeamMember.TeamMemberRole == APTS_Constants.SALES_EXECUTIVE && salesExecUserId == null){
                            salesExecUserId = accountTeamMember.UserId;                    
                        }
                        else if (accountTeamMember.TeamMemberRole == APTS_Constants.ACCOUNT_EXECUTIVE && accountExecUserId == null){
                            accountExecUserId = accountTeamMember.UserId;
                        }
                        else if (accountTeamMember.TeamMemberRole == APTS_Constants.REGIONAL_VP){
                            regionalVpUserId = accountTeamMember.UserId;
                        }
                        if (accountTeamMember.TeamMemberRole == APTS_Constants.SALES_EXEC_PRIMARY){
                            salesExecUserId = accountTeamMember.UserId;                    
                        }
                        if (accountTeamMember.TeamMemberRole == APTS_Constants.ACCOUNT_EXEC_PRIMARY){
                            accountExecUserId = accountTeamMember.UserId;
                        }
                        if (accountTeamMember.TeamMemberRole == APTS_Constants.SALES_CONSULTANT){
                            salesConsultantUserId = accountTeamMember.UserId;
                        }
                        if (accountTeamMember.TeamMemberRole == APTS_Constants.ASSOC_SALES_EXECUTIVE ){
                            secondarySEUserId = accountTeamMember.UserId;
                        }
                        if (accountTeamMember.TeamMemberRole == APTS_Constants.SALES_DIRECTOR  ){
                            salesDirectorUserId = accountTeamMember.UserId;
                        }
                        
                    }
                    proposalObj.Approval_Primary_SE_User__c = salesExecUserId;
                    proposalObj.Approval_Primary_AE_User__c = accountExecUserId;
                    proposalObj.Approval_RVP_User__c = regionalVpUserId;
                    proposalObj.APTPS_Approval_Sales_Consultant_User__c = salesConsultantUserId;
                    proposalObj.APTPS_Approval_Secondary_SE_User__c = secondarySEUserId;
                    proposalObj.Approval_Sales_Director_User__c = salesDirectorUserId;
                }
            }
        }
    }
    
    /** 
    @description: Find out the APEX class implementation for the given productCode
    @param: Product Code
    @return: APEX Class name
    */
    private String findApprovals_Implementation(String productCode) {
        String className = null;
        if(productCode != null && !prodCodeClassNameMap.isEmpty() 
           && prodCodeClassNameMap.containsKey(productCode)) {
               className = prodCodeClassNameMap.get(productCode);
           }
        system.debug('APEX Class Details --> '+className);
        return className;
    }
    
    /** 
    @description: Execute product specific Deal Summary logic.
    @param: Product code and arguments
    @return: 
    */
    public Object call(Map<Id, String> quoteProductMap, Map<Id, Map<String, Object>> quoteArgMap) {
        String retMsg = 'SUCCESS';
        Map<Id, String> quoteClassImplMap = new Map<Id, String>();
        List<String> dsFields = new List<String>();
        Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>> quotePLIMap = new Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>>();
        system.debug('quoteProductMap --> '+quoteProductMap);
        system.debug('quoteArgMap --> '+quoteArgMap);
        
        for(Id qId : quoteProductMap.keySet()){
            String productCode = quoteProductMap.get(qId);
            if(productCode != null) {
                String className = findApprovals_Implementation(productCode);
                quoteClassImplMap.put(qId, className);
            }
            
            //START: Dynamic SOQL for All SNL products
            if(!prodCodeFieldsMap.isEmpty() && prodCodeFieldsMap.containsKey(productCode) && prodCodeFieldsMap.get(productCode) != null) {
                String fieldSetName = prodCodeFieldsMap.get(productCode);
                for(Schema.FieldSetMember fld : Schema.SObjectType.Apttus_QPConfig__ProposalProductAttributeValue__c.fieldSets.getMap().get(fieldSetName).getFields()) {
                    String fldName = 'Apttus_QPConfig__AttributeValueId__r.'+fld.getFieldPath();
                    if(!dsFields.contains(fldName))
                      dsFields.add(fldName);
                }
                
                
            }
            //END
        }
        system.debug('List of fields to retrieve from Proposal Product Attribute Value --> '+dsFields);
        system.debug('quoteClassImplMap --> '+quoteClassImplMap);
        
        //START: Get Proposal Line Item details
        List<Apttus_Proposal__Proposal_Line_Item__c> proposalLineItems = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Set<Id> quoteIds = quoteProductMap.keySet();
        if(!quoteIds.isEmpty()) {
            String query = 'SELECT Id,Apttus_Proposal__Proposal__c,Apttus_Proposal__Product__r.ProductCode,Apttus_QPConfig__AdjustmentType__c,Apttus_QPConfig__AdjustmentAmount__c,Apttus_QPConfig__OptionId__r.ProductCode,'+String.join(dsFields,',')+' FROM Apttus_Proposal__Proposal_Line_Item__c WHERE Apttus_Proposal__Proposal__c IN :quoteIds';
            system.debug('SNL approval attributes query --> '+query);
            proposalLineItems = Database.query(query);
        }
        
        //Generate a Map of Quote ID and related Proposal Line Items
        for(Apttus_Proposal__Proposal_Line_Item__c pli : proposalLineItems) {
            if(!quotePLIMap.isEmpty() && quotePLIMap.containsKey(pli.Apttus_Proposal__Proposal__c))
                quotePLIMap.get(pli.Apttus_Proposal__Proposal__c).add(pli);
            else
                quotePLIMap.put(pli.Apttus_Proposal__Proposal__c, new List<Apttus_Proposal__Proposal_Line_Item__c>{pli});
        }
        system.debug('Quote-Proposal Line Item Map --> '+quotePLIMap);
        //END
        
        List<Apttus_Proposal__Proposal__c> quoteList = new List<Apttus_Proposal__Proposal__c>();
        if(quoteArgMap == null || quoteArgMap.isEmpty()) {
             throw new ExtensionMalformedCallException('Quote/Proposal argument map is empty');
        } else {
            for(Id qId : quoteArgMap.keySet()) {
                Map<String, Object> args = quoteArgMap.get(qId);
                if(args == null || args.get('qId') == null) 
                    throw new ExtensionMalformedCallException('Quote/Proposal Id is missing.');
                quoteList.add((Apttus_Proposal__Proposal__c)args.get('qId'));
            }
        }
        if(!quoteList.isEmpty()) {
            clearApprovalFlags(quoteList);
            updateApproversOnProposal(quoteList);
        }
        
        for(Id qId : quoteClassImplMap.keySet()) {
            String className = quoteClassImplMap.get(qId);
            List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines = !quotePLIMap.isEmpty() && quotePLIMap.containsKey(qId) ? quotePLIMap.get(qId) : null;
            if(className != null) {
                if(quoteArgMap == null || quoteArgMap.isEmpty()) 
                    throw new ExtensionMalformedCallException('Quote/Proposal argument map is empty');
                
                Map<String, Object> args = quoteArgMap.get(qId);
                if(args == null || args.get('qId') == null) 
                    throw new ExtensionMalformedCallException('Quote/Proposal Id is missing.');
                APTPS_ApprovalsInterface approvalObj = (APTPS_ApprovalsInterface)Type.forName(className).newInstance();
                approvalObj.updateApprovals(args, quoteLines);
                
            }       
        }
        setProposalApprovedStatus(quoteList);
        
        return retMsg;
    }
    
    /** 
    @description: Custom Exception implementation
    @param:
    @return: 
    */
    public class ExtensionMalformedCallException extends Exception {
        public String errMsg = '';
        public void ExtensionMalformedCallException(String errMsg) {
            this.errMsg = errMsg;
        }
    }
    
    /** 
    @description: Read Deal Summary Registry metadata and prepare the Map
    @param:
    @return: 
    */
    private void setProdCodeClassNameMap() {
        List<APTPS_Approvals_Registry__mdt> approvalSummaryHandlers = [SELECT Product_Code__c, Class_Name__c,Field_Set__c  
                                                            FROM APTPS_Approvals_Registry__mdt];
        for(APTPS_Approvals_Registry__mdt handler : approvalSummaryHandlers) {
            if(handler.Product_Code__c != null && handler.Class_Name__c != null) 
                prodCodeClassNameMap.put(handler.Product_Code__c, handler.Class_Name__c);
             if(handler.Product_Code__c != null && handler.Class_Name__c != null) 
                prodCodeFieldsMap.put(handler.Product_Code__c, handler.Field_Set__c);
            
        }
        system.debug('prodCodeClassNameMap --> '+prodCodeClassNameMap);
    }
    
    
    
}