/************************************************************************************************************************
@Name: APTPS_CloneEnhancementBatch
@Author: Conga PS Dev Team
@CreateDate: 29 Oct 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CloneEnhancementBatch implements Database.Batchable<sObject>{
    String configID;
    List<Apttus_Config2__LineItem__c> enhancementLines = null;
    public APTPS_CloneEnhancementBatch(String configID, List<Apttus_Config2__LineItem__c> lineItems) {
        this.configID = configID;
        this.enhancementLines = lineItems;
    }
    
    /*public Database.QueryLocator start(Database.BatchableContext BC) {
        String prodCode = 'NJUS_ENHANCEMENT';
        String entitlementLevel = 'Share/Lease Agreement';
        return Database.getQueryLocator('SELECT Id, Apttus_Config2__PrimaryLineNumber__c, Apttus_Config2__AttributeValueId__c, Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreement_Ids__c, Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreements__c FROM Apttus_Config2__LineItem__c WHERE Apttus_Config2__ConfigurationId__c =: configID AND Apttus_Config2__ProductId__r.ProductCode =: prodCode AND Apttus_Config2__IsPrimaryLine__c = true AND Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c =: entitlementLevel');
    }*/
    
    public List<Apttus_Config2__LineItem__c> start(Database.BatchableContext BC) {
        /*String prodCode = 'NJUS_ENHANCEMENT';
        String entitlementLevel = 'Share/Lease Agreement';
        return Database.getQueryLocator('SELECT Id, Apttus_Config2__PrimaryLineNumber__c, Apttus_Config2__AttributeValueId__c, Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreement_Ids__c, Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreements__c FROM Apttus_Config2__LineItem__c WHERE Apttus_Config2__ConfigurationId__c =: configID AND Apttus_Config2__ProductId__r.ProductCode =: prodCode AND Apttus_Config2__IsPrimaryLine__c = true AND Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c =: entitlementLevel');*/
        return enhancementLines;
    }
    
    public void execute(Database.BatchableContext BC, List<Apttus_Config2__LineItem__c> liList) {
        system.debug('Records to process --> '+liList.size());
        Map<Id, double> liPrimaryNumberMap = new Map<Id, double>();
        Map<double, Id> linePrimaryNumberLiMap = new Map<double, Id>();
        Map<Id, List<String>> liUniqueAgreementsMap = new Map<Id, List<String>>();
        Map<Id, Id> liAttrMap = new Map<Id, Id>();
        List<String> globalList = new List<String>();
        for(Apttus_Config2__LineItem__c li : liList) {
            if(li.Apttus_Config2__AttributeValueId__c != null && 
               li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreement_Ids__c != null) {
                   List<String> uniqueList = new List<String>();
                   String selectedIds = li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreement_Ids__c;
                   List<String> selectedAgList = selectedIds.split(',');
                   if(selectedAgList.size() > 1) {
                       for(String ag : selectedAgList) {
                           if(uniqueList.isEmpty())
                               uniqueList.add(ag);
                           else if(!uniqueList.contains(ag))
                               uniqueList.add(ag);
                       }
                       
                       if(uniqueList.size() > 1) {
                           liPrimaryNumberMap.put(li.Id, li.Apttus_Config2__PrimaryLineNumber__c);
                           linePrimaryNumberLiMap.put(li.Apttus_Config2__PrimaryLineNumber__c, li.Id);
                           liUniqueAgreementsMap.put(li.Id, uniqueList);
                           liAttrMap.put(li.Id, li.Apttus_Config2__AttributeValueId__c);
                           globalList.addAll(uniqueList);
                       } else 
                           system.debug('Duplicate values are selected by user.');
                       
                   } else 
                       system.debug('Skip the LI as only one agreement is selected');
                   
               }
        }
        system.debug('liPrimaryNumberMap --> '+liPrimaryNumberMap);
        system.debug('linePrimaryNumberLiMap --> '+linePrimaryNumberLiMap);
        system.debug('liUniqueAgreementsMap --> '+liUniqueAgreementsMap);
        
        List<Apttus__APTS_Agreement__c> agList = [select Id, Name, Apttus__FF_Agreement_Number__c from Apttus__APTS_Agreement__c where Apttus__FF_Agreement_Number__c IN :globalList];
        system.debug('Agreement List --> '+agList);
        Map<String, Map<Id, String>> agDetails = new Map<String, Map<Id, String>>();
        Map<Id, String> agMap = new Map<Id, String>();
        for(Apttus__APTS_Agreement__c ag : agList) {
            agMap.put(ag.Id, ag.Name+' - '+ag.Apttus__FF_Agreement_Number__c);
            if(!agDetails.containsKey(ag.Apttus__FF_Agreement_Number__c))
                agDetails.put(ag.Apttus__FF_Agreement_Number__c, new Map<Id, String>{ag.Id => ag.Name+' - '+ag.Apttus__FF_Agreement_Number__c});
        }
        
        Map<double, String> agLiMap = new Map<double, string>();
        if(!liAttrMap.isEmpty()) {
            if(!liPrimaryNumberMap.isEmpty()) {
                for(Id key : liPrimaryNumberMap.keySet()) {
                    Double lineNumber = liPrimaryNumberMap.get(key);
                    List<String> selectedAgList = liUniqueAgreementsMap.get(key);
                    agLiMap.put(lineNumber, selectedAgList[0]);
                    selectedAgList.remove(0);
                    for(integer i=0; i<selectedAgList.size(); i++) {
                        // Create the request object
                        List<Integer> primaryLineNumbers = new List<Integer>();
                        primaryLineNumbers.add(lineNumber.intValue());
                        Apttus_CPQApi.CPQ.CloneLineItemsRequestDO request = new Apttus_CPQApi.CPQ.CloneLineItemsRequestDO();
                        request.CartId = configID;
                        request.PrimaryLineNumbers = primaryLineNumbers;
                        
                        // Execute the cloneOptionLineItems routine
                        Apttus_CPQApi.CPQ.CloneLineItemsResponseDO response = Apttus_CPQApi.CPQWebService.cloneOptionLineItems(request);
                        for(Apttus_CPQApi.CPQ.IntegerMapDO intMapDO : response.OriginalToCloneMap)
                        {
                            system.debug('Responset Map --> '+intMapDO);
                            System.debug('Source Bundle Line Number = ' + intMapDO.Key + ', Cloned Bundle Line Number = ' + intMapDO.Value);
                            agLiMap.put(intMapDO.Value, selectedAgList[0]);
                            selectedAgList.remove(0);
                        }
                    }
                }
            }
            
            //START: new logic
            List<Apttus_Config2__ProductAttributeValue__c> liToUpdate = new List<Apttus_Config2__ProductAttributeValue__c>();
            if(!agLiMap.isEmpty()) {
                for(Apttus_Config2__LineItem__c li : [SELECT Id, Apttus_Config2__PrimaryLineNumber__c, Apttus_Config2__AttributeValueId__c, Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreement_Ids__c, 
                                                      Apttus_Config2__CopySourceNumber__c, 
                                                      Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreements__c, Apttus_Config2__AttributeValueId__r.APTPS_Agreement__c 
                                                      FROM Apttus_Config2__LineItem__c 
                                                      WHERE Apttus_Config2__ConfigurationId__c =: configID 
                                                      AND Apttus_Config2__PrimaryLineNumber__c IN : agLiMap.keySet()]){
                                                          String agNum = agLiMap.get(li.Apttus_Config2__PrimaryLineNumber__c);
                                                          Map<Id, String> agInfo = agDetails.get(agNum);
                                                          Apttus_Config2__ProductAttributeValue__c attr = new Apttus_Config2__ProductAttributeValue__c(Id = li.Apttus_Config2__AttributeValueId__c);
                                                          Set<Id> keySetId = agInfo.keySet();
                                                          Id agId = (new list<Id>(keySetId))[0];
                                                          String agName = agInfo.get(agId);
                                                          attr.APTPS_Agreement__c = agId;
                                                          attr.APTPS_Selected_Agreement_Ids__c = agNum;
                                                          attr.APTPS_Selected_Agreements__c = agName;
                                                          attr.APTPS_Entitlement_Level__c = 'Share/Lease Agreement';
                                                          attr.APTPS_Override_Agreement_selection__c = false;
                                                          attr.APTPS_Is_Generated_by_Clone_API__c = true;
                                                          liToUpdate.add(attr);
                                                      }
            }
            //END: new logic
            
            if(!liToUpdate.isEmpty()) {
                update liToUpdate;
                
            }
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        system.debug('Finish!');
        Apttus_Config2__ProductConfiguration__c configObj = new Apttus_Config2__ProductConfiguration__c(Id=configID);
        configObj.Apttus_Config2__Status__c = 'Saved';
        update configObj;
    }
}