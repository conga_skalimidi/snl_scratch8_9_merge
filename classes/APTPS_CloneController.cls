/************************************************************************************************************************
@Name: APTPS_CloneController
@Author: Conga PS Dev Team
@CreateDate: 29 Oct 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CloneController {
    private String configID;
    private String proposalID{get;set;}
    private String retId;
    private String configRequestId;
    public Integer totalJobs {get;set;}
    public List<String> jobIds {get;set;}
    public Decimal progressWidth {get;set;}
    public Integer iCount {get;set;}
    public String progressStatus {get;set;}
    public Boolean isEnabled {get;set;}
    public Id propId {get;set;}
    public boolean isDisable {get;set;}
    public boolean isCloneDisabled{get;set;}
    public boolean isSaveDisabled{get;set;}
    public integer intervalTime{get;set;}
    //public Map<String, Map<String, String>> batchProcess {get;set;}
    public Map<String, String> batchProcess {get;set;}
    public APTPS_CloneController(ApexPages.StandardController controller) {
        configID = apexpages.currentpage().getparameters().get('id');
        proposalID = apexpages.currentpage().getparameters().get('businessObjectId');
        retId = apexpages.currentpage().getparameters().get('retId');
        configRequestId = apexpages.currentpage().getparameters().get('configRequestId');
        system.debug('Input param, configID --> '+configID+' proposalID --> '+proposalID+' retId --> '+retId+' configRequestId --> '+configRequestId);
        progressWidth = 0;
        progressStatus = 'Please click Clone to continue.';
        isEnabled = true;
        isDisable = false;
        isCloneDisabled = false;
        isSaveDisabled = true;
        iCount = 0;
        intervalTime = 3;
        batchProcess = new Map<String, String>();
        //For autoclone/onload
        isDisable = true;
        isCloneDisabled = true;
        isSaveDisabled = false;
        progressStatus = 'Cloning initiated...';
        //End onload
    }
    
    public void doClone() {
        isDisable = true;
        isCloneDisabled = true;
        isSaveDisabled = false;
        progressStatus = 'Cloning initiated...';
        String prodCode = 'NJUS_ENHANCEMENT';
        String entitlementLevel = 'Share/Lease Agreement';
        Map<String, List<Apttus_Config2__LineItem__c>> prodLinesMap = new Map<String, List<Apttus_Config2__LineItem__c>>();
        try{
            for(Apttus_Config2__LineItem__c li : [SELECT Id, Apttus_Config2__PrimaryLineNumber__c, Apttus_Config2__AttributeValueId__c, 
                                                  Apttus_Config2__OptionId__r.ProductCode, 
                                                  Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreement_Ids__c, 
                                                  Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreements__c 
                                                  FROM Apttus_Config2__LineItem__c 
                                                  WHERE Apttus_Config2__ConfigurationId__c =: configID 
                                                  AND (Apttus_Config2__ProductId__r.ProductCode =: prodCode OR Apttus_Config2__ProductId__r.ProductCode = 'NJE_ENHANCEMENT') 
                                                  AND Apttus_Config2__IsPrimaryLine__c = true 
                                                  AND Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c =: entitlementLevel]){
                                                      String enhCode = li.Apttus_Config2__OptionId__r.ProductCode;
                                                      if(!prodLinesMap.containsKey(enhCode)) {
                                                          List<Apttus_Config2__LineItem__c> tempList = new List<Apttus_Config2__LineItem__c>();
                                                          tempList.add(li);
                                                          prodLinesMap.put(enhCode, tempList);
                                                      }
                                                      else {
                                                          List<Apttus_Config2__LineItem__c> tempList = prodLinesMap.get(enhCode);
                                                          tempList.add(li);
                                                          prodLinesMap.put(enhCode, tempList);
                                                      }
                                                      
                                                      
                                                  }
            system.debug('Enhancement and Line Items Map --> '+prodLinesMap);
            for(String key : prodLinesMap.keySet()) {
                system.debug('Initiating Clone Batch for --> '+key);
                String jobId = Database.executeBatch(new APTPS_CloneEnhancementBatch(configID, prodLinesMap.get(key)),1);
                jobIds = new List<String>();
                jobIds.add(jobId);
            }
            system.debug('Job Id List --> '+jobIds);
            totalJobs = jobIds.size();
        } catch(Exception e) {
            system.debug('Error while cloning the Enhancements. Error details --> '+e);
            isEnabled = false;
            isDisable = false;
            isCloneDisabled = true;
            isSaveDisabled = false;
            progressStatus = 'Error';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error while cloning the selected Enhancements. Please contact the admin.'));
        }
    }
    
    public PageReference returnToCartPage() {
        PageReference cartDetailPage;
        cartDetailPage = System.Page.Apttus_Config2__Cart;
        //cartDetailPage = System.Page.Apttus_Config2__ProductAttributeDetail3;
        cartDetailPage.getParameters().put('Id', configID);
        cartDetailPage.getParameters().put('configRequestId', configRequestId);
        cartDetailPage.getParameters().put('cartStatus', 'New');
        cartDetailPage.getParameters().put('flow','SNL_Enhancements');
        //cartDetailPage.getParameters().put('launchState','cartgrid');
        return cartDetailPage;
    }
    
    public PageReference saveAndClose() {
        PageReference pg = null;
        String emailAddress = UserInfo.getUserEmail();
        String userId = UserInfo.getUserId();
        system.debug('emailAddress --> '+emailAddress);
        system.debug('Job Ids --> '+jobIds);
        /*String nextExecutionTime = Datetime.now().addSeconds(2).format('s m H d M ? yyyy');  
        System.schedule('CloneAPI_ScheduledJob ' + String.valueOf(Math.random()), nextExecutionTime, 
                        new APTPS_TrackEnhancementCloneBatch(jobId, emailAddress, userId));*/
        String nextExecutionTime = Datetime.now().addSeconds(2).format('s m H d M ? yyyy');  
        System.schedule('CloneAPI_ScheduledJob ' + String.valueOf(Math.random()), nextExecutionTime, 
                        new APTPS_TrackEnhancementCloneBatch(jobIds, emailAddress, userId));
        pg = new PageReference('/'+proposalID);
       	pg.setRedirect(true);
        return pg;
    }
    
    public void checkStatus() {
        PageReference pg = null;
        system.debug('Check Status for Job Id --> '+jobIds);
        try{
            if(jobIds!= null && !jobIds.isEmpty()) {
                //intervalTime = 20;
                List<AsyncApexJob> jobs = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE Id IN :jobIds];
                //system.debug('Status --> '+job.Status);
                
                progressWidth = Decimal.valueOf(iCount) != null ? Decimal.valueOf(iCount) * 100 : 99;
                system.debug('progressWidth --> '+progressWidth);
                //Map<String, String> tempMap = new Map<String, String>();
                //tempMap.put(jobId, job.Status);
                //batchProcess.put('Non Standard', job.Status);
                Integer index = 0;
                for(AsyncApexJob job : jobs) {
                    if('Completed'.equalsIgnoreCase(job.Status))
                        jobIds.remove(index);
                    index += 1;
                }
                
                if(jobIds.size() == 0) {
                    progressStatus = 'Completed';
                    progressWidth = 100;
                    isEnabled = false;
                    isDisable = false;
                    //redirectToAg(agId);
                    isCloneDisabled = true;
                    isSaveDisabled = false;
                }
                else {
                    progressStatus = 'Cloning...';
                    iCount = jobIds.size()/totalJobs;
                    //iCount += 5;
                }  
            } else if(!'Completed'.equalsIgnoreCase(progressStatus)) {
                doClone();
            }
        } catch(Exception e) {
            system.debug('Error while tracking the batch status. Error details --> '+e);
            isEnabled = false;
            isDisable = false;
            isCloneDisabled = true;
            isSaveDisabled = false;
            progressStatus = 'Error';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error while cloning the selected Enhancements. Please contact the admin.'));
        }
    }
}