/************************************************************************************************************************
@Name: APTPS_Agreement_Base_TriggerHandler
@Author: Conga PS Dev Team
@CreateDate:
@Description: Agreement Trigger Handler. As per trigger event, execute respective business logic from APTPS_Agreement_Helper
helper class.
************************************************************************************************************************
@ModifiedBy: Conga PS Dev Team
@ModifiedDate: 17/11/2020
@ChangeDescription: Added logic for Shares and Leases.
************************************************************************************************************************/
public class APTPS_Agreement_Base_TriggerHandler extends TriggerHandler{
    public override void afterInsert(){
        List<Apttus__APTS_Agreement__c> allAgList = Trigger.new;
        Map<Id, Apttus__APTS_Agreement__c> newMap = (Map<Id, Apttus__APTS_Agreement__c>)Trigger.NewMap;
        Map<Id, Id> agQuoteMap = new Map<Id, Id>();
        List<Apttus__APTS_Agreement__c> SNLAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> otherAgList = new List<Apttus__APTS_Agreement__c>();
        Id onHoldRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.INTERIM_AGREEMENT_HOLD).getRecordTypeId();
        
        for(Apttus__APTS_Agreement__c ag : allAgList) {
            //GCM-7127: TODO, Discuss about it. Adding this condition to avoid recurssion
            if(onHoldRecType != ag.RecordTypeId) {
                if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    SNLAgList.add(ag);
                    if(ag.Apttus_QPComply__RelatedProposalId__c != null)
                        agQuoteMap.put(ag.Id, ag.Apttus_QPComply__RelatedProposalId__c);
                } else {
                    otherAgList.add(ag );
                }
            }
        }
        if(!SNLAgList.isEmpty()) {
            APTPS_Agreement_Helper.createSNLDocusignRecipients(SNLAgList);
            APTPS_Agreement_Helper.setSNLAgreementExtensionFields(SNLAgList); 
        }
        if(!otherAgList.isEmpty()) {
            APTPS_Agreement_Helper.createDocusignRecipients(otherAgList); 
            APTPS_Agreement_Helper.setAgreementExtensionFields(otherAgList);
        }
        //CLM Automation
        APTPS_Agreement_Helper.handleCLMAutomation(Trigger.new);
        
        //START: GCM-7127, Interim Lease agreement
        if(!agQuoteMap.isEmpty())
            APTPS_Agreement_Helper.handleInterimLease(agQuoteMap);
        //END: GCM-7127
    }
    
    public override void beforeInsert(){
        List<Apttus__APTS_Agreement__c> allAgList = Trigger.new;
        APTPS_Agreement_Helper.setAgreementFields(Trigger.new);
        APTPS_Agreement_Helper.createAgreementExtensionRecord(Trigger.new);
        List<Apttus__APTS_Agreement__c> demoAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> ctAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> SNLAgList = new List<Apttus__APTS_Agreement__c>();
        Id onHoldRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.INTERIM_AGREEMENT_HOLD).getRecordTypeId();
        
        for(Apttus__APTS_Agreement__c ag : allAgList) {
            ag.Apttus__VersionAware__c = false;
            if(onHoldRecType != ag.RecordTypeId && (APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c) ||  APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.LEASE.equalsIgnoreCase(ag.APTPS_Program_Type__c))) {
                SNLAgList.add(ag);
            }
        }
        system.debug('SNLAgList-->'+SNLAgList+' allAgList-->'+allAgList);
        
        if(!SNLAgList.isEmpty()) {
            system.debug('SNLAgList-->'+SNLAgList);
            APTPS_Agreement_Helper.stampSNLAgreementFields(SNLAgList); 
        }   
    }
    
    public override void afterUpdate(){
        Map<Id, Apttus__APTS_Agreement__c> oldMap = (Map<Id, Apttus__APTS_Agreement__c>)Trigger.OldMap;
        List<Apttus__APTS_Agreement__c> newList = (List<Apttus__APTS_Agreement__c>)Trigger.new;
        String oldAgStatus, oldAgStatusCat, newAgStatus,oldAgFundStatus,newAgFundStatus;
        APTPS_EmailNotification enObj = new APTPS_EmailNotification();
        
        Map<Id,List<Apttus__APTS_Agreement__c>> agrParentMap = new Map<Id,List<Apttus__APTS_Agreement__c>>();
        List<Apttus__APTS_Agreement__c> updateChildAgrList = new List<Apttus__APTS_Agreement__c>();
        set<id> agrIds = new set<id>();
        
        for (Apttus__APTS_Agreement__c agr : newList){ 
            if(agr.Apttus__Parent_Agreement__c == null){
                agrIds.add(agr.id);
            }
        }
        
        for(Apttus__APTS_Agreement__c childAgreementObj : [SELECT Id, Apttus__Parent_Agreement__c,Agreement_Status__c,Funding_Status__c, Chevron_Status__c, APTS_Path_Chevron_Status__c, Document_Status__c, Apttus__Status__c, Apttus__Status_Category__c FROM Apttus__APTS_Agreement__c WHERE Apttus__Parent_Agreement__c IN :agrIds]) {
             if(agrParentMap.containsKey(childAgreementObj.Apttus__Parent_Agreement__c)) {
                List<Apttus__APTS_Agreement__c> listAgr = agrParentMap.get(childAgreementObj.Apttus__Parent_Agreement__c);
                listAgr.add(childAgreementObj);
                agrParentMap.put(childAgreementObj.Apttus__Parent_Agreement__c,listAgr);
            } else {
                agrParentMap.put(childAgreementObj.Apttus__Parent_Agreement__c,new List<Apttus__APTS_Agreement__c>{childAgreementObj});
            }
            
        }
        
        for (Apttus__APTS_Agreement__c ag : newList){ 
            Apttus__APTS_Agreement__c oldAgObj = oldMap.get(ag.Id);
            //START: Compare Agreement Prior details
            oldAgStatus = oldAgObj.Apttus__Status__c;
            oldAgStatusCat = oldAgObj.Apttus__Status_Category__c;
            newAgStatus = ag.Agreement_Status__c;
            oldAgFundStatus = oldAgObj.Funding_Status__c;
            newAgFundStatus = ag.Funding_Status__c;
            //Agreement Funded Contract 
            if (!APTS_ConstantUtil.FUNDED_CONTRACT.equalsIgnoreCase(oldAgFundStatus) && APTS_ConstantUtil.FUNDED_CONTRACT.equalsIgnoreCase(ag.Funding_Status__c)){
                if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    try{
                        String Business_Object = APTS_ConstantUtil.AGREEMENT_OBJ;
                        String Product_Code = APTS_ConstantUtil.CT_PRODUCT_CODE;
                        String Status = ag.Funding_Status__c;
                        String objId = ag.id;
                        String contactId = ag.Apttus__Primary_Contact__c;
                        enObj.call(Product_Code, new Map<String, Object>{'Id' => objId,'Business_Object'=>Business_Object,'Product_Code'=>Product_Code,'Status'=>Status,'ContactId'=>contactId});
                        
                    }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                        system.debug('Error while sending Email Notification for Agreement Id --> '+ag.id+' Error details --> '+e.errMsg);
                    }
                }
            }
            //Agreement Activaiton
            if (!APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(oldAgStatus) && !APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(oldAgStatusCat) && APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(ag.Apttus__Status__c) && APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(ag.Apttus__Status_Category__c)){
                if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    try{
                        String Business_Object = APTS_ConstantUtil.AGREEMENT_OBJ;
                        String Product_Code = '';
                        if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                            Product_Code = APTS_ConstantUtil.CT_PRODUCT_CODE;
                        } else if(APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                            if(APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(ag.CurrencyIsoCode) ) {
                                Product_Code = APTS_ConstantUtil.NJUS_DEMO;
                            } else if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode) ) {
                                Product_Code = APTS_ConstantUtil.NJE_DEMO;
                            }
                            
                        }               
                        String Status = ag.Apttus__Status__c;
                        String objId = ag.id;
                        String contactId = ag.Apttus__Primary_Contact__c;
                        enObj.call(Product_Code, new Map<String, Object>{'Id' => objId,'Business_Object'=>Business_Object,'Product_Code'=>Product_Code,'Status'=>Status,'ContactId'=>contactId});
                        
                    }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                        system.debug('Error while sending Email Notification for Agreement Id --> '+ag.id+' Error details --> '+e.errMsg);
                    }
                }else if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    //Terminate Child Interim Lease Agreement
                    if(agrParentMap.containsKey(ag.id)) {
                        for(Apttus__APTS_Agreement__c childAgr : agrParentMap.get(ag.id)){
                            childAgr = APTPS_SNL_Util.setAgreementStatus(childAgr, APTS_ConstantUtil.AGREEMENT_TERMINATED, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_SIGNED);
                            childAgr.Apttus__Status__c = APTS_ConstantUtil.AGREEMENT_TERMINATED;
                            childAgr.Apttus__Status_Category__c = APTS_ConstantUtil.AGREEMENT_TERMINATED;
                            //childAgr.Agreement_Status__c = APTS_ConstantUtil.AGREEMENT_TERMINATED;
                            childAgr.Apttus__Termination_Date__c = ag.Apttus__Activated_Date__c;
                            updateChildAgrList.add(childAgr);
                        }
                        
                    }
                }
                
                
                
            }
        }
        if(!updateChildAgrList.isEmpty()) {
            try{
                update updateChildAgrList;
            }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                system.debug('Error while updating Child Agreements. Error details --> '+e.errMsg);
            }
        }
    }
    
    public override void beforeUpdate() {
        //START: SNL Agreement lifecycle management
        List<Apttus__APTS_Agreement__c> newAg = Trigger.New;
        List<Apttus__APTS_Agreement__c> demoNewAgList =  new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> demoNJANewAgList =  new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> ctNewAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> ctAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> shareNJANewAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> shareNJENewAgList = new List<Apttus__APTS_Agreement__c>();
        Map<Id,SObject> oldAgMap = Trigger.OldMap;
        Map<Id,SObject> demoOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> demoNJAOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> ctOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> shareNJAOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> shareNJEOldAgMap = new Map<Id,SObject>();
        Id onHoldRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.INTERIM_AGREEMENT_HOLD).getRecordTypeId();
        for(Apttus__APTS_Agreement__c ag : newAg) {
            if(APTPS_SNL_Util.isSNLProgramType(ag.APTPS_Program_Type__c)) {
                //NJE Demo product - Agreement
                if(APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c)){
                    if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                        demoNewAgList.add(ag);
                        demoOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    } else {
                        demoNJANewAgList.add(ag);
                        demoNJAOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    }
                }               
                //Corporate Trial - Agreement
                if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    ctNewAgList.add(ag);
                    ctOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                }
                //Share Product - Agreement
                if(onHoldRecType != ag.RecordTypeId && APTS_ConstantUtil.SHARE.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                        shareNJENewAgList.add(ag);
                        shareNJEOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    } else {
                        shareNJANewAgList.add(ag);
                        shareNJAOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    }
                }
            }
        }
        //NJE Demo product - Agreement Status update
        if(!demoNewAgList.isEmpty()) {
            system.debug('demoNewAgList-->'+demoNewAgList);
            system.debug('demoOldAgMap-->'+demoOldAgMap);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJE_DEMO  ,new Map<String, Object>{'newAgrList'=>demoNewAgList,'oldAgrList'=>demoOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Demo-->'+e.errMsg);
            }
            
        }
        //NJA Demo product - Agreement Status update
        if(!demoNJANewAgList.isEmpty()) {
            system.debug('demoNJANewAgList-->'+demoNJANewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJUS_DEMO   ,new Map<String, Object>{'newAgrList'=>demoNJANewAgList,'oldAgrList'=>demoNJAOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Demo-->'+e.errMsg);
            }
            
        }
        
        //Corporate Trial - Agreement Status update
        if(!ctNewAgList.isEmpty()) {
            system.debug('ctNewAgList-->'+ctNewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.CT_PRODUCT_CODE ,new Map<String, Object>{'newAgrList'=>ctNewAgList,'oldAgrList'=>ctOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Corporate Trail-->'+e.errMsg);
            }
            
        }
        
        //Share NJA - Agreement Status update
        if(!shareNJANewAgList.isEmpty()) {
            system.debug('shareNJANewAgList-->'+shareNJANewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJA_SHARE_CODE ,new Map<String, Object>{'newAgrList'=>shareNJANewAgList,'oldAgrList'=>shareNJAOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Share -->'+e.errMsg);
            } 
        }
        
        //Share NJE - Agreement Status update
        if(!shareNJENewAgList.isEmpty()) {
            system.debug('shareNJENewAgList-->'+shareNJENewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJE_SHARE_CODE ,new Map<String, Object>{'newAgrList'=>shareNJENewAgList,'oldAgrList'=>shareNJEOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Share -->'+e.errMsg);
            } 
        }
        //END: SNL Agreement lifecycle
    }
}