/************************************************************************************************************************
@Name: APTPS_TrackEnhancementCloneBatch
@Author: Conga PS Dev Team
@CreateDate: 29 Oct 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_TrackEnhancementCloneBatch implements Schedulable {
    //String batchId;
    String emailId;
    String userId;
    List<String> jobIds;
    
    public APTPS_TrackEnhancementCloneBatch(List<String> jobIds, String email, String userId) {
        this.jobIds = jobIds;
        //this.emailId = email;
        this.emailId = 'abamane@conga.com';
        this.userId = userId;
    }
    
    public void execute(SchedulableContext sc) {
        //TODO: Check-should we add executed/created by condition. This is will help to reduce the time wait.
        //system.debug('batchId -->'+batchId);
        system.debug('userId --> '+userId);
        /*Integer jobs = [SELECT count() FROM AsyncApexJob WHERE ApexClass.name = 'APTPS_CloneBatch' 
AND CreatedById =: userId 
AND Status != 'Completed'];*/
        if(jobIds != null) {
            Integer jobs = [SELECT count() FROM AsyncApexJob 
                            WHERE Id IN :jobIds 
                            AND Status != 'Completed'];
            system.debug('jobs --> '+jobs);
            if(jobs == 0) {
                //Send an email
                Messaging.reserveSingleEmailCapacity(1);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {emailId};
                    mail.setToAddresses(toAddresses);
                mail.setReplyTo(emailId);
                mail.setSenderDisplayName('Support');
                mail.setSubject('Configuration has been cloned and saved');
                mail.setBccSender(false);
                mail.setUseSignature(false);
                mail.setPlainTextBody('Your configuration has been cloned and saved.');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            } else{
                //TODO: Read this time from custom settings if required.
                String nextExecutionTime = Datetime.now().addSeconds(5).format('s m H d M ? yyyy');  
                System.schedule('CloneAPI_ScheduledJob ' + String.valueOf(Math.random()), nextExecutionTime, 
                                new APTPS_TrackEnhancementCloneBatch(jobIds, emailId, userId));
            }
        }
    }
}