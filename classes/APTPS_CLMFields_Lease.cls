/************************************************************************************************************************
@Name: APTPS_CLMFields_Lease
@Author: Conga PS Dev Team
@CreateDate: 19 Oct 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMFields_Lease {
    /** 
    @description: ppoulate Agreeement Fields
    @param: AgreementList and Currency Code
    @return: 
    */
    private static void populateLeaseFields(List<Apttus__APTS_Agreement__c> agrList, String currencyCode) {
        Id leaseRecTypeId;
        if(currencyCode == APTS_ConstantUtil.CUR_EUR ) {
            leaseRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.LEASE_NJE_RECORD_TYPE ).getRecordTypeId();
        } else if(currencyCode == APTS_ConstantUtil.CUR_USD ) {
            leaseRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.LEASE_NJA_RECORD_TYPE).getRecordTypeId();
        }
        
        for(Apttus__APTS_Agreement__c agObj : agrList) {
            agObj.RecordTypeId = leaseRecTypeId;
        }
    }
    
    /** 
    @description: NJE Lease implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CLMFieldsInterface {
        public void populateCLMFields(Object args) {
            system.debug('Populate Agreement Fileds');
            List<Apttus__APTS_Agreement__c> agrList = (List<Apttus__APTS_Agreement__c>)args;
            APTPS_CLMFields_Lease.populateLeaseFields(agrList,APTS_ConstantUtil.CUR_EUR);
        }
    }
    
    /** 
    @description: NJA Lease implementation
    @param:
    @return: 
    */
    public class NJA_Fields implements APTPS_CLMFieldsInterface {
        public void populateCLMFields(Object args) {
            system.debug('Populate Agreement Fileds');
            List<Apttus__APTS_Agreement__c> agrList = (List<Apttus__APTS_Agreement__c>)args;
            APTPS_CLMFields_Lease.populateLeaseFields(agrList,APTS_ConstantUtil.CUR_USD);
        }
    }
}