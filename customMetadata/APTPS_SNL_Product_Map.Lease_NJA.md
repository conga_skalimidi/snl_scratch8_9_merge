<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Lease NJA</label>
    <protected>false</protected>
    <values>
        <field>CurrencyCode__c</field>
        <value xsi:type="xsd:string">USD</value>
    </values>
    <values>
        <field>Product_Code__c</field>
        <value xsi:type="xsd:string">NJUS_LEASE</value>
    </values>
    <values>
        <field>Program_Type__c</field>
        <value xsi:type="xsd:string">Lease</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:type="xsd:string">APTPS_NJA_Lease_Proposal</value>
    </values>
</CustomMetadata>
