<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Lease NJUS</label>
    <protected>false</protected>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">APTPS_CLMFields_Lease.NJA_Fields</value>
    </values>
    <values>
        <field>Product_Code__c</field>
        <value xsi:type="xsd:string">NJUS_LEASE</value>
    </values>
</CustomMetadata>
