<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Occupied Hourly Fee 2</label>
    <protected>false</protected>
    <values>
        <field>Age_of_Aircraft__c</field>
        <value xsi:type="xsd:double">60.0</value>
    </values>
    <values>
        <field>Charge_Type__c</field>
        <value xsi:type="xsd:string">Occupied Hourly Fee 2</value>
    </values>
</CustomMetadata>
