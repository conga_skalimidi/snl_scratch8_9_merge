<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Request Aircraft Positioning</label>
    <protected>false</protected>
    <values>
        <field>Agreement_Action__c</field>
        <value xsi:type="xsd:string">Request Aircraft Positioning</value>
    </values>
    <values>
        <field>Agreement_Status__c</field>
        <value xsi:type="xsd:string">Pending Positioning</value>
    </values>
    <values>
        <field>Approval_Status__c</field>
        <value xsi:type="xsd:string">Not Submitted,Approved</value>
    </values>
    <values>
        <field>Document_Status__c</field>
        <value xsi:type="xsd:string">Document Received,Document Signed</value>
    </values>
    <values>
        <field>Funding_Status__c</field>
        <value xsi:type="xsd:string">Funded Contract</value>
    </values>
    <values>
        <field>Status_Category__c</field>
        <value xsi:type="xsd:string">In Authoring,In Signatures</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">Author Contract, Other Party Review, Approved Request, Other Party Signatures, Fully Signed</value>
    </values>
</CustomMetadata>
