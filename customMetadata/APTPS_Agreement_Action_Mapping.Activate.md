<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Activate</label>
    <protected>false</protected>
    <values>
        <field>Agreement_Action__c</field>
        <value xsi:type="xsd:string">Activate</value>
    </values>
    <values>
        <field>Agreement_Status__c</field>
        <value xsi:type="xsd:string">Pending Activation</value>
    </values>
    <values>
        <field>Approval_Status__c</field>
        <value xsi:type="xsd:string">Approved</value>
    </values>
    <values>
        <field>Document_Status__c</field>
        <value xsi:type="xsd:string">Document Signed</value>
    </values>
    <values>
        <field>Funding_Status__c</field>
        <value xsi:type="xsd:string">Funded Contract</value>
    </values>
    <values>
        <field>Status_Category__c</field>
        <value xsi:type="xsd:string">In Signatures</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">Fully Signed</value>
    </values>
</CustomMetadata>
