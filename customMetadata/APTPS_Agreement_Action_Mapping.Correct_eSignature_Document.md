<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Correct eSignature Document</label>
    <protected>false</protected>
    <values>
        <field>Agreement_Action__c</field>
        <value xsi:type="xsd:string">Correct eSignature Document</value>
    </values>
    <values>
        <field>Agreement_Status__c</field>
        <value xsi:type="xsd:string">Contract Pending</value>
    </values>
    <values>
        <field>Approval_Status__c</field>
        <value xsi:type="xsd:string">Approved</value>
    </values>
    <values>
        <field>Document_Status__c</field>
        <value xsi:type="xsd:string">Document Not Signed,Document Received</value>
    </values>
    <values>
        <field>Funding_Status__c</field>
        <value xsi:type="xsd:string">Pending Funding</value>
    </values>
    <values>
        <field>Status_Category__c</field>
        <value xsi:type="xsd:string">In Signatures</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">Other Party Signatures</value>
    </values>
</CustomMetadata>
