<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Preview &amp; Submit Approvals</label>
    <protected>false</protected>
    <values>
        <field>Agreement_Action__c</field>
        <value xsi:type="xsd:string">Preview &amp; Submit Approvals</value>
    </values>
    <values>
        <field>Agreement_Status__c</field>
        <value xsi:type="xsd:string">In Review,In Approvals,Contract Generated</value>
    </values>
    <values>
        <field>Approval_Status__c</field>
        <value xsi:type="xsd:string">Not Submitted,Pending Approval,Rejected,Cancelled,Approval Required</value>
    </values>
    <values>
        <field>Document_Status__c</field>
        <value xsi:type="xsd:string">Document Generated</value>
    </values>
    <values>
        <field>Funding_Status__c</field>
        <value xsi:type="xsd:string">Pending Funding</value>
    </values>
    <values>
        <field>Status_Category__c</field>
        <value xsi:type="xsd:string">In Authoring</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">Ready for Signatures,Other Party Review,Pending Approval,Approved Request,Rejected Approval,Cancelled Approval</value>
    </values>
</CustomMetadata>
